﻿using System;
using System.Windows.Input;

namespace FluentValidationWithWPF.WpfApp.Core
{
    public class DelegateCommand : ICommand
    {
        private readonly Action commandAction;

        #pragma warning disable 67
        public event EventHandler CanExecuteChanged;
        #pragma warning restore 67

        public DelegateCommand(Action action)
        {
            commandAction = action;
        }

        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter) => commandAction?.Invoke();
    }
}
