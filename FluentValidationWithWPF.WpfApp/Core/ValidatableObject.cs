﻿using FluentValidation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace FluentValidationWithWPF.WpfApp.Core
{
    public abstract class ValidatableObject<T> : ObservableObject, INotifyDataErrorInfo
    {
        private bool hasErrors;
        private Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();
        private IValidator<T> validator;

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public ValidatableObject(IValidator<T> validator)
        {
            this.validator = validator;
        }

        public IEnumerable GetErrors(string propertyName)
        {
            if (errors.ContainsKey(propertyName))
                return errors[propertyName];

            return null;
        }

        public bool HasErrors
        {
            get { return hasErrors; }
            private set { Set(ref hasErrors, value); }
        }

        protected bool SetAndTriggerValidation<TField>(
            ref TField field, TField value, [CallerMemberName] string propertyName = null)
        {
            var result = Set(ref field, value, propertyName);

            if (result) 
                ValidateInstance();

            return result;
        }

        private void ValidateInstance()
        {
            var validationResults = validator.Validate(this);
            HasErrors = !validationResults.IsValid;
            errors.Clear();

            foreach (var validationResult in validationResults.Errors)
            {
                if (errors.ContainsKey(validationResult.PropertyName))
                    errors[validationResult.PropertyName].Add(validationResult.ErrorMessage);
                else
                    errors.Add(validationResult.PropertyName, new List<string> { validationResult.ErrorMessage });
            }

            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(errors.Keys.FirstOrDefault()));
        }
    }
}
