﻿using FluentValidation;
using FluentValidationWithWPF.WpfApp.Core;
using FluentValidationWithWPF.WpfApp.Models;

namespace FluentValidationWithWPF.WpfApp.Validators
{
    public class ContactModelValidator : AbstractValidator<ContactModel>
    {
        public ContactModelValidator()
        {
            RuleFor(contact => contact.Name)
                .NotEmpty();

            RuleFor(contact => contact.Email)
                .EmailAddress();
        }
    }
}
