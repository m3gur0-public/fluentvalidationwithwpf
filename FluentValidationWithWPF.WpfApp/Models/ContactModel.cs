﻿using FluentValidation;
using FluentValidationWithWPF.WpfApp.Core;
using FluentValidationWithWPF.WpfApp.Validators;

namespace FluentValidationWithWPF.WpfApp.Models
{
    public class ContactModel : ValidatableObject<ContactModel>
    {
        private string name;
        private string email;

        public ContactModel() : base(new ContactModelValidator()) { }

        public string Name
        {
            get { return name; }
            set { SetAndTriggerValidation(ref name, value); }
        }

        public string Email
        {
            get { return email; }
            set { SetAndTriggerValidation(ref email, value); }
        }
    }
}
