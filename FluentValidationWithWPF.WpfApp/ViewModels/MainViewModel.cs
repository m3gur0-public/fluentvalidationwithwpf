﻿using FluentValidationWithWPF.WpfApp.Core;
using FluentValidationWithWPF.WpfApp.Models;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;

namespace FluentValidationWithWPF.WpfApp.ViewModels
{
    public class MainViewModel : ObservableObject
    {
        private ContactModel selectedContact;

        public ObservableCollection<ContactModel> Contacts { get; } = new ObservableCollection<ContactModel>();

        public ContactModel SelectedContact
        {
            get { return selectedContact; }
            set { Set(ref selectedContact, value); }
        }

        public ICommand AddContactCommand { get; }

        public MainViewModel()
        {
            AddContactCommand = new DelegateCommand(OnAddContact);
        }

        private void OnAddContact()
        {
            Trace.WriteLine("Adding contact");

            var contact = new ContactModel();
            Contacts.Add(contact);
            SelectedContact = contact;
        }
    }
}
